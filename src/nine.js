"use strict";

/**
 * Find target as sum in range
 * @param range
 * @param target
 * @return {boolean}
 */
function findSum(range, target) {
  for (let i = 0; i < range.length; i++) {
    for (let j = 0; j < range.length; j++) {
      if (i !== j && range[i] + range[j] === target) {
        return true;
      }
    }
  }
  return false;
}

/**
 * Decode cypher text
 * @param cypher
 * @param preamble
 * @return {number}
 */
function decode(cypher, preamble) {
  for (let i = preamble; i < cypher.length - preamble; i++) {
    const code = cypher[i];
    const range = cypher.slice(i - preamble, i);
    if (!findSum(range, code)) {
      return code;
    }
  }
  return 0;
}

/**
 * Attack the cypher text
 * @param cypher
 * @param vector
 * @return {{low: *, height: *}}
 */
function attack(cypher, vector) {
  const sets = [];
  let combined = 0;
  for (let iv = 0, i = 0; i < cypher.length; ) {
    combined += cypher[i];
    if (combined === vector && Math.abs(iv - i) >= 2) {
      const lowToHigh = cypher.slice(iv, i).sort((a, b) => a - b);
      sets.push({
        low: lowToHigh.shift(),
        high: lowToHigh.pop(),
      });
    }
    if (combined > vector) {
      combined = 0;
      ++iv;
      i = iv;
    } else {
      i++;
    }
  }
  return sets.shift();
}

module.exports = {
  decode,
  attack,
};
