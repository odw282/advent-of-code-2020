"use strict";

/**
 * Lookup trees in a forest for a given path
 * @param forrest {Array<string>}
 * @param right {number}
 * @param down {number}
 * @return {{map: *, trees: number}}
 */
function lookupTrees(forrest, right, down) {
  const map = forrest.map((line) => line.split(""));
  const mapWidth = map[0].length;
  let x = 0,
    y = 0,
    trees = 0;
  while (y < map.length - 1) {
    // Move
    x += right;
    y += down;

    // lookup map position
    if (map[y][x % mapWidth]) {
      const target = map[y][x % mapWidth];
      if (target === "#") {
        trees += 1;
        map[y][x % mapWidth] = "X";
      } else {
        map[y][x % mapWidth] = "O";
      }
    }
  }
  return {
    map,
    trees,
  };
}

module.exports = {
  lookupTrees,
};
