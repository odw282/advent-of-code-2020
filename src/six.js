"use strict";
const { readFileIntoArray } = require("./util/fs");

/**
 *
 * @param path
 * @param separator
 * @return {[*[], number]}
 */
function readGroupAnswers(path = "input/six.txt", separator = "") {
  return readFileIntoArray(path).reduce(
    ([lines, index], line) => {
      if (line.length > 0) {
        if (!lines[index]) lines[index] = line;
        else lines[index] += separator + line;
      } else {
        ++index;
      }
      return [lines, index];
    },
    [[], 0]
  );
}

/**
 * Count occurrence of each character in list of strings
 * @param group
 * @return {{[key:string]: number}}
 */
function getFrequency(group) {
  let freq = {};
  for (const answer of group) {
    for (let i = 0; i < answer.length; i++) {
      const character = answer.charAt(i);
      if (freq[character]) {
        freq[character]++;
      } else {
        freq[character] = 1;
      }
    }
  }
  return freq;
}

/**
 * Count the characters that occur a specific amount of times.
 * @param frequency {{[key:string]: number}}
 * @param occurrence {number}
 * @return {number}
 */
function countCommonFrequency(frequency, occurrence) {
  let total = 0;
  for (const key in frequency) {
    const count = frequency[key];
    if (count === occurrence) {
      ++total;
    }
  }
  return total;
}

module.exports = {
  readGroupAnswers,
  getFrequency,
  countCommonFrequency,
};
