"use strict";

/**
 * Sum amount values and compare with needed
 * @param amounts
 * @param needed
 * @return {number}
 */
function sumTwoToNeeded(amounts, needed) {
  // Loop over all amounts twice
  for (const left of amounts) {
    for (const right of amounts) {
      if (left + right === needed) {
        return left * right;
      }
    }
  }
}

/**
 * Sum amount values
 * @param amounts
 * @param needed
 * @return {number}
 */
function sumThreeToNeeded(amounts, needed) {
  for (const left of amounts) {
    for (const middle of amounts) {
      for (const right of amounts) {
        if (left + middle + right === needed) {
          return left * middle * right;
        }
      }
    }
  }
}

module.exports = {
  sumTwoToNeeded,
  sumThreeToNeeded,
};
