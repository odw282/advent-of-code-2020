/**
 * Instruction constants
 */
const FORWARD = "F";
const LEFT = "L";
const RIGHT = "R";
const NORTH = "N";
const EAST = "E";
const SOUTH = "S";
const WEST = "W";
const DIRECTIONS = [EAST, SOUTH, WEST, NORTH];

/**
 * Sail towards a waypoint
 * @param instructions
 * @return {{manhattan: number, position: {x: number, y: number}}}
 */
function sailWaypoint(instructions) {
  // set initial position
  let waypoint = {
    x: 10,
    y: -1,
  };

  // set initial position
  let position = {
    x: 0,
    y: 0,
  };

  // loop over every instruction
  for (let i = 0; i < instructions.length; i++) {
    let [instruction, command, value] = /([A-Z])([0-9]+)/g.exec(
      instructions[i]
    );
    value = parseInt(value);
    switch (command) {
      case FORWARD:
        position = moveShip(waypoint, position, value);
        break;
      case LEFT:
      case RIGHT:
        waypoint = steerWaypoint(command, waypoint, value);
        break;
      default:
        waypoint = move(command, waypoint, value);
    }
  }
  return {
    position,
    manhattan: Math.abs(position.x) + Math.abs(position.y),
  };
}

/**
 *
 * @param waypoint
 * @param position
 * @param value
 */
function moveShip(waypoint, position, value) {
  return {
    x: position.x + waypoint.x * value,
    y: position.y + waypoint.y * value,
  };
}

/**
 * Steer the waypoint
 *
 * @param direction
 * @param waypoint
 * @param value
 */
function steerWaypoint(direction, waypoint, value) {
  const distance = Math.sqrt(
    Math.pow(Math.abs(waypoint.x), 2) + Math.pow(Math.abs(waypoint.y), 2)
  );
  const radians = Math.atan2(waypoint.y, waypoint.x);
  const radiansToMove = (value / 180) * Math.PI;
  const nextAngle =
    direction === LEFT ? radians - radiansToMove : radians + radiansToMove;
  return {
    x: Math.round(Math.cos(nextAngle) * distance),
    y: Math.round(Math.sin(nextAngle) * distance),
  };
}

/**
 * Sail as per instructions
 * @param instructions
 * @return {manhattan: number, position: {x: number, y: number}}
 */
function sailDirection(instructions) {
  // set initial sailing direction
  let direction = 0;

  // set initial position
  let position = {
    x: 0,
    y: 0,
  };

  // loop over every instruction
  for (let i = 0; i < instructions.length; i++) {
    let [instruction, command, value] = /([A-Z])([0-9]+)/g.exec(
      instructions[i]
    );
    value = parseInt(value);
    switch (command) {
      case FORWARD:
        position = move(DIRECTIONS[direction], position, value);
        break;
      case LEFT:
      case RIGHT:
        direction = steerDirection(command, direction, value);
        break;
      default:
        position = move(command, position, value);
    }
  }
  return {
    position,
    manhattan: Math.abs(position.x) + Math.abs(position.y),
  };
}

/**
 * Steer ship in direction
 * @param command
 * @param direction
 * @param value
 * @return {*}
 */
function steerDirection(command, direction, value) {
  const turnsToMake = value / 90;
  for (let i = 0; i < turnsToMake; i++) {
    if (command === LEFT) {
      direction = direction - 1 < 0 ? DIRECTIONS.length - 1 : direction - 1;
    } else {
      direction = direction + 1 >= DIRECTIONS.length ? 0 : direction + 1;
    }
  }
  return direction;
}

/**
 * Move amount into direction
 * @param towards
 * @param position
 * @param value
 * @return {*}
 */
function move(towards, position, value) {
  switch (towards) {
    case NORTH:
      position.y -= value;
      break;
    case EAST:
      position.x += value;
      break;
    case SOUTH:
      position.y += value;
      break;
    case WEST:
      position.x -= value;
      break;
  }
  return position;
}

module.exports = {
  sailDirection,
  sailWaypoint,
};
