"use strict";

/**
 * Build the bag rule set
 * @param lines
 * @return {{}}
 */
function buildRuleSetA(lines) {
  const rules = {};
  for (const line of lines) {
    const [rawBag, rawContents] = line.split(" contain ");
    const [attr, color] = rawBag.split(" ");
    const contents = rawContents.split(", ");
    const bag = `${attr} ${color}`;
    rules[bag] = [];
    for (const content of contents) {
      const [bagCount, bagAttr, bagColor] = content.split(" ");
      const color = `${bagAttr} ${bagColor}`;
      if (bagCount !== "no") {
        if (rules[bag].find((b) => b === color) === undefined)
          rules[bag].push(color);
      }
    }
  }
  return rules;
}

/**
 * Can the fit bag be stored in our outer bag?
 * @param outerBag
 * @param fitBag
 * @return {boolean|*}
 */
function canContainBag(rules, outerBag, fitBag) {
  if (rules[outerBag].find((b) => b === fitBag) !== undefined) {
    return true;
  } else {
    return rules[outerBag].reduce(
      (fitFound, innerBag) => fitFound | canContainBag(rules, innerBag, fitBag),
      false
    );
  }
}

/**
 * Build the bag rule set
 * @param lines
 * @return {{}}
 */
function buildRuleSetB(lines) {
  const rules = {};
  for (const line of lines) {
    const [rawBag, rawContents] = line.split(" contain ");
    const [attr, color] = rawBag.split(" ");
    const contents = rawContents.split(", ");
    const bag = `${attr} ${color}`;
    rules[bag] = [];
    for (const content of contents) {
      const [bagCount, bagAttr, bagColor] = content.split(" ");
      const color = `${bagAttr} ${bagColor}`;
      if (bagCount !== "no") {
        for (let i = 0; i < bagCount; i++) {
          rules[bag].push(color);
        }
      }
    }
  }
  return rules;
}

/**
 * Count the number of bags needed
 * @param rules
 * @param bag
 * @return {number|*}
 */
function countBags(rules, bag) {
  if (rules[bag].length > 0) {
    return (
      rules[bag].length +
      rules[bag].reduce(
        (total, content) => total + countBags(rules, content),
        0
      )
    );
  } else {
    return 0;
  }
}

module.exports = {
  countBags,
  canContainBag,
  buildRuleSetA,
  buildRuleSetB,
};
