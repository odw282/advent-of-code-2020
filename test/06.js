const test = require("ava");
const {
  readGroupAnswers,
  getFrequency,
  countCommonFrequency,
} = require("../src/six");

test("Day 6: Custom Customs - Test case A", (t) => {
  const [answers] = readGroupAnswers("test/six/test.txt");
  const total = answers.reduce((total, answer) => {
    return total + [...new Set(answer)].length;
  }, 0);
  t.assert(total === 11);
});

test("Day 6: Custom Customs - Solution A", (t) => {
  const [answers] = readGroupAnswers("input/six.txt");
  const total = answers.reduce((total, answer) => {
    return total + [...new Set(answer)].length;
  }, 0);
  t.log("Answer:", total);
  t.pass();
});

test("Day 6: Custom Customs - Test case B", (t) => {
  const [answers] = readGroupAnswers("test/six/test.txt", " ");
  const total = answers.reduce((total, answer) => {
    const group = answer.split(" ");
    const freq = getFrequency(group);
    const count = countCommonFrequency(freq, group.length);
    return total + count;
  }, 0);
  t.assert(total === 6);
});

test("Day 6: Custom Customs - Solution B", (t) => {
  const [answers] = readGroupAnswers("input/six.txt", " ");
  const total = answers.reduce((total, answer) => {
    const group = answer.split(" ");
    const freq = getFrequency(group);
    const count = countCommonFrequency(freq, group.length);
    return total + count;
  }, 0);
  t.log("Answer:", total);
  t.pass();
});
