const test = require("ava");
const {
  readPassports,
  isValidPassport,
  isCompletePassport,
} = require("../src/four");

test("Day 4: Passport Processing - Test case A", (t) => {
  const [passports] = readPassports("test/four/valid.txt");
  const validCount = passports.reduce(
    (valid, passport, index) =>
      isValidPassport(passport.split(" ")) ? ++valid : valid,
    0
  );
  t.assert(validCount === passports.length);
});

test("Day 4: Passport Processing - Solution A", (t) => {
  const [passports] = readPassports("input/four.txt");
  const validCount = passports.reduce(
    (valid, passport, index) =>
      isCompletePassport(passport.split(" ")) ? ++valid : valid,
    0
  );
  t.log("Answer:", validCount);
  t.pass();
});

test("Day 4: Passport Processing - Test case B", (t) => {
  const [passports] = readPassports("test/four/invalid.txt");
  const invalidCount = passports.reduce(
    (valid, passport, index) =>
      isValidPassport(passport.split(" ")) ? valid : ++valid,
    0
  );
  t.assert(invalidCount === passports.length);
});

test("Day 4: Passport Processing - Solution B", (t) => {
  const [passports] = readPassports("input/four.txt");
  const validCount = passports.reduce(
    (valid, passport, index) =>
      isValidPassport(passport.split(" ")) ? ++valid : valid,
    0
  );
  t.log(`Answer:`, validCount);
  t.pass();
});
