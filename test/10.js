const test = require("ava");
const { chain, combinations } = require("../src/ten");
const { readFileIntoArray } = require("../src/util/fs");

test("Day 10: Adapter Array - Test case A", (t) => {
  const adapters = readFileIntoArray("test/ten/testBig.txt").map((i) =>
    parseInt(i)
  );
  const count = chain(adapters);
  t.log("count", count);
  t.assert(count === 22 * 10);
});

test("Day 10: Adapter Array - Solution A", (t) => {
  const adapters = readFileIntoArray("input/ten.txt").map((i) => parseInt(i));
  t.log("Answer", chain(adapters));
  t.pass();
});

test("Day 10: Adapter Array - Test case B - small", (t) => {
  const adapters = readFileIntoArray("test/ten/testSmall.txt").map((i) =>
    parseInt(i)
  );
  const count = combinations(adapters);
  t.log("count", count);
  t.assert(count === 8);
});

test("Day 10: Adapter Array - Test case B - Big", (t) => {
  const adapters = readFileIntoArray("test/ten/testBig.txt").map((i) =>
    parseInt(i)
  );
  const count = combinations(adapters);
  t.log("count", count);
  t.assert(count === 19208);
});

test("Day 10: Adapter Array - Solution B", (t) => {
  const adapters = readFileIntoArray("input/ten.txt").map((i) => parseInt(i));
  t.log("Answer:", combinations(adapters));
  t.pass();
});
