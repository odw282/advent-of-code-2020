const test = require('ava');
const {readFileIntoArray} = require("../src/util/fs");

const ADD = '+';
const MUL = '*';
const OPEN = '(';
const CLOSE = ')';

function findInnerExpression(expression) {
  let start = -1;
  for (let i = 0; i < expression.length; i++) {
    const char = expression[i];
    if (char === OPEN) {
      start = i;
    } else if (char === CLOSE) {
      return [start, i+1]
    }
  }
}

function calculateA(expression) {
  const splitPlus = expression.split('+').flatMap(s => [s, '+']);
  splitPlus.pop();
  const splitMultiply = splitPlus.flatMap(s => {
    const multiply = s.split('*').flatMap(s => [s, '*'])
    multiply.pop();
    return multiply;
  });
  while (splitMultiply.length > 1) {
    const takeExpression = splitMultiply.splice(0, 3);
    splitMultiply.unshift(eval(takeExpression.join('')));
  }
  return splitMultiply[0];
}

function calculateB(expression) {
  const splitPlus = expression.split('+').flatMap(s => [s, '+']);
  splitPlus.pop();
  const splitMultiply = splitPlus.flatMap(s => {
    const multiply = s.split('*').flatMap(s => [s, '*'])
    multiply.pop();
    return multiply;
  });

  while (splitMultiply.indexOf(ADD) !== -1) {
    const index = splitMultiply.indexOf(ADD);
    const takeExpression = splitMultiply.splice(index - 1, 3);
    splitMultiply.splice(index - 1, 0,  eval(takeExpression.join('')));
  }

  while (splitMultiply.indexOf(MUL) !== -1) {
    const index = splitMultiply.indexOf(MUL);
    const takeExpression = splitMultiply.splice(index - 1, 3);
    splitMultiply.splice(index - 1, 0,  eval(takeExpression.join('')));
  }
  return splitMultiply[0];
}

function evaluateExpression(expression, fn) {
  expression = expression.replaceAll(' ','');
  while (true) {
    if (expression.indexOf(OPEN) !== -1) {
      const [a, b] = findInnerExpression(expression);
      const toEval = expression.slice(a + 1, b - 1);
      const calculated = fn(toEval);
      const tmpArr = Array.from(expression);
      tmpArr.splice(a, b - a, calculated);
      expression = tmpArr.join('');
    } else {
      return fn(expression);
    }
  }
  return 0;
}

test('Day 18: Operation Order - Test case A', t => {
  t.assert(evaluateExpression('5 + (8 * 3 + 9 + 3 * 4 * 3)', calculateA) === 437);
  t.assert(evaluateExpression('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))', calculateA) === 12240);
  t.assert(evaluateExpression('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2', calculateA) === 13632);
});


test('Day 18: Operation Order - Solution A', t => {
  const lines = readFileIntoArray('input/eighteen.txt');
  t.log('Answer:', lines.reduce((total, line) => total + evaluateExpression(line, calculateA), 0));
  t.pass();
});

test('Day 18: Operation Order - Test case B', t => {
  t.assert(evaluateExpression('1 + (2 * 3) + (4 * (5 + 6))', calculateB) === 51);
  t.assert(evaluateExpression('2 * 3 + (4 * 5)', calculateB) === 46);
  t.assert(evaluateExpression('5 + (8 * 3 + 9 + 3 * 4 * 3)', calculateB) === 1445);
  t.assert(evaluateExpression('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))', calculateB) === 669060);
  t.assert(evaluateExpression('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2', calculateB) === 23340);
});

test('Day 18: Operation Order - Solution B', t => {
  const lines = readFileIntoArray('input/eighteen.txt');
  t.log('Answer:', lines.reduce((total, line) => total + evaluateExpression(line, calculateB), 0));
  t.pass();
});
