const test = require("ava");
const { lastSpoken } = require("../src/fifteen");

test("Day 15: Rambunctious Recitation - Test case A", (t) => {
  const inputs = [
    "0,3,6",
    "1,3,2",
    "2,1,3",
    "1,2,3",
    "2,3,1",
    "3,2,1",
    "3,1,2",
  ];
  const answers = [436, 1, 10, 27, 78, 438, 1836];
  inputs.forEach((i, index) => {
    t.assert(lastSpoken(i, 2019) === answers[index]);
  });
});
/*
test('Day 15: Rambunctious Recitation - Solution A', t => {
  const input = '14,8,16,0,1,17';
  t.log('Answer:' , lastSpoken(input, 2019));
  t.pass();
});

test('Day 15: Rambunctious Recitation - Test case B', t => {
  const inputs = ['0,3,6', '1,3,2', '2,1,3', '1,2,3', '2,3,1', '3,2,1', '3,1,2'];
  const answers = [175594, 2578, 3544142, 261214, 6895259, 18, 362]
  inputs.forEach((i, index) => {
    t.assert(lastSpoken2(i, 29999999) === answers[index]);
  });
});*/

test("Day 15: Rambunctious Recitation - Solution B", (t) => {
  const input = "14,8,16,0,1,17";
  t.log("Answer:", lastSpoken(input, 29999999));
  t.pass();
});
